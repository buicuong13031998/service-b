import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { AmqpModule } from './microservices/amqp.module';
@Module({
  imports: [
    AmqpModule,
    ConfigModule.forRoot({
      envFilePath: '.env.development',
      ignoreEnvFile: process.env.NODE_ENV === 'development' ? false : true,
      isGlobal: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
