/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import {
  AmqpConnection,
  Nack,
  RabbitRPC,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq';
import { AmqpProducer } from './amqp.producer';

@Injectable()
export class AmqpConsumer {
  constructor() {}

  @RabbitSubscribe({
    exchange: 'team_name.project_name',
    routingKey: 'message.cmd.receive.service-a.service-b',
    queue: 'team_name.project_name-message.cmd.receive.service-a.service-b',
  })
  public async receiveMessage(message: any) {
    console.log(message);
  }

  @RabbitRPC({
    exchange: 'team_name.project_name',
    routingKey: 'message.cmd.send-rpc.service_a.service_b',
    queue: 'team_name.project_name-message.cmd.send-rpc.service_a.service_b',
  })
  public async receiveRpc(data) {
    try {
      console.log(data);
      return true;
    } catch (error) {
      return new Nack();
    }
  }
}
