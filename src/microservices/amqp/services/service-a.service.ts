/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable } from '@nestjs/common';
import { AmqpProducer } from '../../amqp.producer';
@Injectable()
export class ServiceAService {
  constructor(private readonly _amqpProducer: AmqpProducer) {}

  public async sendMessage(message: string): Promise<any> {}
}
